import datetime
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas
from kivy.app import App
from kivy.garden.matplotlib import FigureCanvasKivyAgg
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.checkbox import CheckBox
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.lang import Builder
from kivy.uix.textinput import TextInput
from sklearn import linear_model
from sklearn.metrics import r2_score, accuracy_score, mean_squared_error
matplotlib.use("module://kivy.garden.matplotlib.backend_kivy")

Builder.load_string("""
<MainWindow>:
    size_hint: .8, .8
    pos_hint: {'center_x': .5, 'center_y': .5}
    do_default_tab: False
    tab_width: 200
    on_current_tab: app.onTabChange(self.current_tab)    
    TabbedPanelItem:
        text: 'Login'
        
        BoxLayout:
            id:login
            orientation: 'vertical'
            padding: "150dp"
            
            TextInput:
                id: username
                hint_text: 'Username'
                font_size: '18sp'
                pos_hint: {'center_x': .5, 'center_y': .5}
                size_hint: None, None
                size: 300, 50                
            
            TextInput:
                id: password
                hint_text: 'Password'
                password:True
                pos_hint: {'center_x': .5, 'center_y': .5}
                font_size: '18sp'
                size: 300, 50
                size_hint: None, None
                
            Button:
                text: 'Login'
                pos_hint: {'center_x': .5, 'center_y': .5}
                font_size: '18sp'
                size: 300, 50
                size_hint: None, None
                on_press: app.login_method(username.text, password.text)
                
    TabbedPanelItem:
        text:'Independent variables'
        
        BoxLayout:
            orientation: 'vertical'
            id: features
            Label:
                size_hint_y: .1
                text: 'Select the variables that you want to deal with them'
    
    TabbedPanelItem:
        text: 'Results'
        BoxLayout:
            id: results
            orientation: 'vertical'
            BoxLayout:
                size_hint_y: .1
                Button:
                    text: 'Linear Regression'
                    on_press: app.train_model()

            Label:
                id: show_results
                size_hint_y: .2
            BoxLayout:
                id: show_results_plot
                size_hint_y: .7
    TabbedPanelItem:
        text: 'Predictions' 
        BoxLayout:
            id: predictions 
            orientation: 'vertical'
            size_hint_y: .3
""")


class MainWindow(TabbedPanel):
    pass


class TabbedPanelApp(App):
    def build(self):
        self.t = MainWindow()
        self.dataset_path = ''
        self.instances = []
        self.targets = []
        self.grid = None
        self.dropdown = None
        self.middle_label = None
        self.box_layout_plot = None
        self.figure_results = None
        self.figure = None
        self.model = None
        self.all_features = []
        self.grid_predictions = None
        self.text_boxes = []
        return self.t

    def login_method(self, username_, password_, ):
        if username_ != '' and password_ != '':
            if username_ == 'student' and password_ == '12345':
                self.dataset_path = 'Dataset/dataset.csv'
                self.popup_methd()
                print(self.t.tab_list[1])
                self.t.switch_to(self.t.tab_list[2])
            else:
                self.popup_method_retry()
        else:
            self.popup_method_retry()

    def popup_methd(self):
        btn_msg_valid = Button(text='Okay')
        popup_message = Popup(title='Login successfully.File uploaded...',
                              content=btn_msg_valid, size_hint=(None, None), size=(400, 400),
                              auto_dismiss=False)
        btn_msg_valid.bind(on_press=popup_message.dismiss)
        popup_message.open()

    def popup_method_retry(self):
        btn_msg = Button(text='Please try again')
        popup_msg = Popup(title='Invalid Credentials. Please try again...', content=btn_msg, size_hint=(None, None), size=(400, 400), auto_dismiss=False)
        btn_msg.bind(on_press=popup_msg.dismiss)
        popup_msg.open()

    def onTabChange(self, tab):
        if tab.text == 'Independent variables':
            if self.dataset_path:
                if self.grid and self.box_layout_plot and self.middle_label:
                    self.t.ids.features.remove_widget(self.grid)
                    self.t.ids.features.remove_widget(self.box_layout_plot)
                    self.t.ids.features.remove_widget(self.dropdown)
                    self.t.ids.features.remove_widget(self.middle_label)
                    if self.figure:
                        self.box_layout_plot.remove_widget(self.figure)
                file = open(self.dataset_path, 'r')
                self.grid = GridLayout(cols=6, col_default_width=.3, size_hint_y=.1)
                features = set()
                for i, line in enumerate(file.readlines()):
                    if i == 0:
                        ww = line.split(';')
                        features = ww[1:len(ww) - 1]
                        break
                for feature in features:
                    self.grid.add_widget(Label(text=feature))
                    active = CheckBox(active=True)
                    self.grid.add_widget(active)
                    self.all_features.append(feature)
                self.t.ids.features.add_widget(self.grid)

                self.box_layout_plot = BoxLayout()
                self.dropdown = DropDown(size_hint_y=1., dismiss_on_select=False, auto_dismiss=False)
                for k, feature in enumerate(features):
                    if k < len(features) - 1:
                        btn = Button(text=str(feature), size_hint_y=None, height=44)
                        btn.bind(on_release=lambda btn: self.show_plot(btn.text))
                        self.dropdown.add_widget(btn)
                self.middle_label = Label(text='Choose a variable to see the distribution of the classes', size_hint_y=.1)

                self.t.ids.features.add_widget(self.middle_label)
                self.box_layout_plot.add_widget(self.dropdown)
                self.t.ids.features.add_widget(self.box_layout_plot)
        elif tab.text == 'Results':
            if self.dataset_path:
                self.instances = []
                self.targets = []
                self.dates = []
                self.indexes = []
                self.model = None
                counter = 0
                for checkbox in reversed(self.grid.children):
                    if isinstance(checkbox, CheckBox):
                        print(checkbox.active)
                        if checkbox.active:
                            self.indexes.append(self.all_features[counter])
                        counter += 1
                print(self.indexes)

                file = open(self.dataset_path, 'r')
                ind = []
                for i, line in enumerate(file.readlines()):
                    if i == 0:
                        features = line.split(';')
                        for j, feature in enumerate(features):
                            if feature in self.indexes:
                                ind.append(j)
                    if i > 0:
                        features = line.split(';')
                        x = []
                        for j, feature in enumerate(features):
                            if j in ind:
                                x.append(float(feature))
                        self.dates.append(datetime.datetime.strptime(features[0], '%d/%m/%Y'))
                        label = features[len(features) - 1]
                        self.instances.append(x)
                        self.targets.append(label)

        elif tab.text == 'Predictions':
            if self.model:
                if self.grid_predictions:
                    self.grid_predictions.clear_widgets()
                    self.grid_predictions_buttons.clear_widgets()
                    self.t.ids.predictions.clear_widgets()
                self.grid_predictions = GridLayout(cols=12, col_default_width=.8, size_hint_y=.1)
                self.text_boxes = []
                for index in self.indexes:
                    self.grid_predictions.add_widget(Label(text=index))
                    text_box = TextInput(size_hint_y=.1)
                    self.grid_predictions.add_widget(text_box)
                    self.text_boxes.append(text_box)
                btn = Button(text='Prediction')
                btn.bind(on_release=lambda btn: self.predict_model())
                self.grid_predictions_buttons = GridLayout(cols=2, col_default_width=.8, size_hint_y=.1)
                self.grid_predictions_buttons.add_widget(btn)
                self.label_prediction = Label(text='The prediction is')
                self.grid_predictions_buttons.add_widget(self.label_prediction)
                self.t.ids.predictions.add_widget(self.grid_predictions)
                self.t.ids.predictions.add_widget(self.grid_predictions_buttons)

    def shuffle_in_unison_scary(self, a, b):
        rng_state = np.random.get_state()
        np.random.shuffle(a)
        np.random.set_state(rng_state)
        np.random.shuffle(b)

    def __get_instances(self):
        a = np.array(self.instances).astype('float64')
        b = np.array(self.targets).astype('float64')
        return a, b

    def show_plot(self, text):
        if self.dataset_path:
            if self.figure:
                self.box_layout_plot.remove_widget(self.figure)
            file = open(self.dataset_path, 'r')
            indexes = []
            prices = []
            ind = ''
            for i, line in enumerate(file.readlines()):
                if i == 0:
                    features = line.split(';')
                    for j in range(len(features)):
                        if features[j].find(text) >= 0:
                            ind = j
                if i > 0:
                    features = line.split(';')
                    indexes.append(datetime.datetime.strptime(features[0], '%d/%m/%Y'))
                    prices.append(float(features[ind]))
            df = pandas.DataFrame({'x': indexes, 'y1': prices})

            plt.plot('x', 'y1', data=df, marker='', markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
            plt.title(text)
            self.figure = FigureCanvasKivyAgg(plt.gcf())
            self.box_layout_plot.add_widget(self.figure)
            plt.close()

    def train_model(self):
        if self.figure_results:
            self.t.ids.show_results_plot.clear_widgets()
            self.figure_results = None
        instances, targets = self.__get_instances()
        train_instances, train_targets = instances[:int(len(instances) * 80 / 100)], targets[:int(len(targets) * 80 / 100)]
        test_instances, test_targets = instances[int(len(instances) * 80 / 100):], targets[int(len(targets) * 80 / 100):]
        train_dates, test_dates = self.dates[int(len(instances) * 80 / 100):], self.dates[int(len(targets) * 80 / 100):]
        l_m = linear_model.LinearRegression()
        self.model = l_m.fit(train_instances, train_targets)
        predictions = self.model.predict(test_instances)

        true_y = []
        for y in test_targets:
            if y >= 0:
                true_y.append(1)
            else:
                true_y.append(0)

        predicted_y = []
        for y in predictions:
            if y >= 0:
                predicted_y.append(1)
            else:
                predicted_y.append(0)

        # todo r2_score
        self.t.ids.show_results.text = 'Total coef. %f\n Acc.: %f\n MSE: %f\n R^2: %f\n The selected features: %s' % \
                                       (self.model.score(train_instances, train_targets),
                                        accuracy_score(true_y, predicted_y),
                                        mean_squared_error(test_targets, predictions),
                                        r2_score(test_targets, predictions), ','.join(self.indexes))

        a_plot = BoxLayout(orientation='vertical')
        df = pandas.DataFrame({'x': test_dates, 'true value': test_targets, 'predictions': predictions})
        plt.plot('x', 'true value', data=df, marker='', markerfacecolor='blue', markersize=12, color='skyblue',
                 linewidth=2)
        plt.plot('x', 'predictions', data=df, marker='', color='olive', linewidth=0.5)
        plt.legend()
        self.figure_results = FigureCanvasKivyAgg(plt.gcf())
        a_plot.add_widget(self.figure_results)
        self.t.ids.show_results_plot.add_widget(a_plot)
        plt.close()

    def predict_model(self):
        x = []
        for text_field in self.text_boxes:
            x.append(float(text_field.text))
        X = []
        X.append(x)
        Y = self.model.predict(X)
        self.label_prediction.text = 'The prediction is ' + str(Y[0])


if __name__ == '__main__':
    TabbedPanelApp().run()