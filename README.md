# Prediction for the rise or the decline of global financial market by using Linear Regression

This is a project in the context of Master Thesis of University of Pireaus.
Supervised machine learning by using linear regression technique.

## Details

The scope of this thesis was the development and the evaluation of linear regression for the
prediction of the international stock market MSCI rise/decline. Particularly, we assumed that
there is a linear correlation between the MSCI index and the most important indices around
the world along with the polarity of the news (i.e. if the news are positives or negatives). The
indices we used are, the stock market of Frankfort (DAX), the stock market of Paris (CAC 40),
the Dow Jones, the stock market of London (FTSE 100), the NASDAQ and the S&P 500.

The findings of the experiments, conducted in the context of the thesis, are interesting both
for the various stock market indices studied as well as for the contribution of additional
information from the collected texts.

Finally, the thesis is accompanied by a graphical user interface, which enables people who are
interested to this topic to train their own linear regression models, loading training sets.
People can also predict the stock market rise or decline by giving real time prices to the
independent variables.

For more information click [here](http://dione.lib.unipi.gr/xmlui/handle/unipi/12632).

## Usage

PyCharm 2019 JetBrains - https://www.jetbrains.com/pycharm/ <br />
Python 3.7 <br />
Project Interpreter (Virtual Environment)

### Main Python packages:

* Matplotlib 3.1.2
* Numpy 1.17.3
* Pandas 0.25.3
* Scikit-learn 0.21.3
* Kivy 1.11.1
* Kivy-garden 0.1.4
* Kivy-garden.graph 0.4.0